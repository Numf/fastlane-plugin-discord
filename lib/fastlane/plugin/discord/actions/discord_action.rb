require 'fastlane/action'
require_relative '../helper/discord_helper'
require 'net/https'
require 'uri'
require 'json'

module Fastlane
  module Actions
    class DiscordAction < Action
      def self.run(params)

        url = "https://discordapp.com/api/webhooks/#{params[:discord_id]}/#{params[:discord_token]}"
        uri = URI.parse(url)

        header = {'Content-Type': 'application/json'}
        json = {content: params[:message],
                username: params[:user_name]
        }

# Create the HTTP objects
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        request = Net::HTTP::Post.new(uri.request_uri, header)
        request.body = json.to_json

# Send the request
        response = http.request(request)


      end

      def self.description
        "post message to discord"
      end

      def self.authors
        ["Alexander"]
      end

      def self.return_value
        # If your method provides a return value, you can describe here what it does
      end

      def self.details
        # Optional:
        "post message to discord"
      end

      def self.available_options
        [
            FastlaneCore::ConfigItem.new(key: :message,
                                         env_name: "DISCORD_MESSAGE",
                                         description: "Post message",
                                         optional: true,
                                         type: String),
            FastlaneCore::ConfigItem.new(key: :user_name,
                                         env_name: "DISCORD_USER_NAME",
                                         description: "User Name",
                                         optional: true,
                                         type: String),
            FastlaneCore::ConfigItem.new(key: :discord_id,
                                         env_name: "DISCORD_ID",
                                         description: "Id of discord webhook",
                                         optional: false,
                                         type: String),
            FastlaneCore::ConfigItem.new(key: :discord_token,
                                         env_name: "DISCORD_TOKEN",
                                         description: "Token of discord webhook",
                                         optional: false,
                                         type: String)
        ]
      end

      def self.is_supported?(platform)
        # Adjust this if your plugin only works for a particular platform (iOS vs. Android, for example)
        # See: https://docs.fastlane.tools/advanced/#control-configuration-by-lane-and-by-platform
        #
        # [:ios, :mac, :android].include?(platform)
        true
      end
    end
  end
end
